import 'package:app/config/theme/color_theme.dart';
import 'package:app/ui/screens/blind_game_screen.dart';
import 'package:app/ui/screens/history_screen.dart';
import 'package:app/ui/widgets/confirm_restart_modal.dart';
import 'package:app/ui/widgets/first_launch_modal.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BottomNavMain extends StatefulWidget {
  @override
  _BottomNavMainState createState() => _BottomNavMainState();
}

class _BottomNavMainState extends State<BottomNavMain>
    with AutomaticKeepAliveClientMixin<BottomNavMain> {
  int _selectedIndex = 0;
  final _controller = PageController(initialPage: 0);

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  final List<Widget> _pages = [
    BlindGameScreen(),
    HistoryScreen(),
  ];

  late SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _loadPrefs();
    });
  }

  _loadPrefs() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('tutoShown') != true) {
      bool shouldProceed = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return FirstLaunchModal(); // Display your custom dialog
        },
      );
      prefs.setBool('tutoShown', true);
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    _controller.animateToPage(
      index,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
          physics: const PageScrollPhysics(),
          children: _pages,
          onPageChanged: _onItemTapped,
          controller: _controller),
      bottomNavigationBar: Container(
        height: 80,
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.20),
        child: BottomNavigationBar(
          backgroundColor: Colors.transparent,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                backgroundColor: Colors.transparent,
                icon: Image(
                  image: AssetImage('assets/pawn_disabled.png'),
                  width: 32,
                  height: 32,
                ),
                activeIcon: Image(
                  image: AssetImage('assets/pawn.png'),
                  width: 32,
                  height: 32,
                ),
                label: ''),
            BottomNavigationBarItem(
              backgroundColor: Colors.transparent,
              icon: Icon(Icons.history),
              label: '',
            ),
          ],
          currentIndex: _selectedIndex,
          elevation: 0,
          selectedItemColor: Colors.black87,
          unselectedItemColor: THEME_DARKGRAY,
          onTap: _onItemTapped,
          type: BottomNavigationBarType.fixed,
          iconSize: 24,
          showSelectedLabels: false, // Hide selected labels
          showUnselectedLabels: false, // Hide un
        ),
      ),
    );
  }
}
