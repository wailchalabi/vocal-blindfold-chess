import 'package:app/config/theme/color_theme.dart';
import 'package:app/models/colors.dart';
import 'package:app/models/move_played_return.dart';
import 'package:app/models/sentence.dart';
import 'package:app/models/uci_move.dart';
import 'package:app/providers/game.dart';
import 'package:app/providers/sql_helper.dart';
import 'package:app/providers/stockfish.dart';
import 'package:app/providers/tts.dart';
import 'package:app/ui/screens/preferences_screen.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:app/ui/widgets/chessboard_modal_button.dart';
import 'package:app/ui/widgets/choose_color_modal.dart';
import 'package:app/ui/widgets/confirm_restart_modal.dart';
import 'package:app/ui/widgets/current_game_status.dart';
import 'package:app/ui/widgets/end_game_modal.dart';
import 'package:app/ui/widgets/game_turn_status.dart';
import 'package:app/ui/widgets/play_move_modal_button.dart';
import 'package:app/ui/widgets/speech_to_text_input.dart';
import 'package:app/ui/widgets/tutorial_bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chess_board/flutter_chess_board.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockfish/stockfish.dart';
import 'package:wakelock/wakelock.dart';

class BlindGameScreen extends StatefulWidget {
  BlindGameScreen({super.key});

  @override
  State<BlindGameScreen> createState() => _BlindGameScreenState();
}

class _BlindGameScreenState extends State<BlindGameScreen> {
  late StockfishAI ia;
  late Game game;
  final TtsManager _ttsManager = TtsManager();
  late SharedPreferences prefs;

  GameResult? gameRes;
  bool loading = false;

  _loadPrefs() async {
    // await SQLHelper.deleteDatabaseFile();
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    Wakelock.enable();
    super.initState();
    game = Game.instance;
    ia = new StockfishAI(game: game, onEnd: endGameTrigger);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _loadPrefs();
    });
  }

  Future<void> stopGame(MovePlayedReturn? moveReturn) async {
    setState(() {
      loading = true;
    });
    if (game.chess.move_number <= 1) return;
    int id = await SQLHelper.createGame(
        game.currentPgn,
        gameResultString(moveReturn != null && moveReturn.result != null
            ? moveReturn.result!
            : GameResult.LOOSE),
        prefs.getInt('stockfish') ?? 1,
        getColorLabel(game.playerColor!),
        game.chess.move_number);
    if (ia.state == StockfishState.ready) {
      game.stopGame();
    }
    setState(() {
      loading = false;
    });
  }

  void stopGameDialog() async {
    bool shouldProceed = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return ConfirmRestartModal(); // Display your custom dialog
      },
    );
    if (shouldProceed) {
      await stopGame(null); //
    }
  }

  @override
  void dispose() {
    // Execute your function here
    Wakelock.disable();
    super.dispose();
  }

  void startGame() async {
    this.gameRes = null;
    if (ia.iaSingleton.stockfish?.state == StockfishState.ready) return;

    PlayerColor chosenColor = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return ChooseColorModal(); // Display your custom dialog
      },
    );
    game.startNewGame(chosenColor);
    game.iaTurn = chosenColor == PlayerColor.white ? false : true;
    setState(() {
      loading = true;
    });
    await ia.create();
    ia.iaSingleton.stockfish?.stdin = "ucinewgame";
    if (chosenColor == PlayerColor.black) {
      ia.sendCommand(game.chess.fen, game.historyString);
    }
    setState(() {
      loading = false;
    });
  }

  Future<void> endGameTrigger(MovePlayedReturn moveReturn) async {
    bool? analyze = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return EndGameModal(
            res: moveReturn.result!); // Display your custom dialog
      },
    );
    if (analyze == true) {
      openAnalysisBoard(game.currentPgn, game.playerColor!, context);
    }
    await stopGame(moveReturn);
  }

  void submitStockfish(dynamic input) async {
    MovePlayedReturn moveReturn = game.playPlayerMove(input);
    if (moveReturn.isLegal) {
      ia.sendCommand(game.chess.fen, game.historyString);
    } else {
      _ttsManager.speak('Coup illégal');
    }
    if (moveReturn.result != null) {
      await endGameTrigger(moveReturn);
    }
  }

  void handleNewSpeech(String speech) {
    Sentence sentence = Sentence(rawSentence: speech, game: game);
    if (sentence.parsedMove != null) {
      UciMove uciMove = parseUCIMove(sentence.parsedMove!);
      submitStockfish(uciMove.toJson());
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: game,
      child: ChangeNotifierProvider.value(
        value: ia,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.settings_outlined),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => PreferencesScreen(
                      title: 'Preferences',
                    ),
                  ),
                );
              },
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: const Text(
              'Partie',
              style: TextStyle(
                  fontWeight: FontWeight.w900, fontFamily: 'Readex Pro'),
            ),
            actions: [
              IconButton(
                  onPressed: stopGameDialog,
                  icon: const Icon(Icons.restart_alt)),
            ],
          ),
          body: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            Expanded(
              child: CurrentGameStatus(loading: loading),
            ),
            !game.gameOn
                ? Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 48, vertical: 8),
                    width: MediaQuery.of(context).size.width,
                    child: UIButton(
                      text: 'Jouer',
                      textColor: THEME_LIGHT,
                      onPressed: startGame,
                      image: const AssetImage('assets/chess_pawn.png'),
                      background: Theme.of(context).colorScheme.secondary,
                    ),
                  )
                : Column(
                    children: [
                      GameTurnStatus(),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            PlayMoveModalButton(
                              context: context,
                              game: game,
                              onPlayMove: submitStockfish,
                            ),
                            const SizedBox(width: 8),
                            SpeechToTextInput(onSpeechChange: handleNewSpeech),
                          ],
                        ),
                      ),
                      TutorialBubble()
                    ],
                  ),
            ChessboardModalButton(context: context, game: game),
          ]),
        ),
      ),
    );
  }
}
