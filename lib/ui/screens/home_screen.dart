import 'package:app/providers/game.dart';
import 'package:app/providers/stockfish.dart';
import 'package:app/ui/screens/blind_game_screen.dart';
import 'package:app/ui/screens/history_screen.dart';
import 'package:app/ui/screens/preferences_screen.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:app/ui/widgets/current_game_status.dart';
import 'package:app/ui/widgets/speech_to_text_input.dart';
import 'package:app/ui/widgets/stockfish_status.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockfish/stockfish.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _loadPrefs();
    });
  }

  _loadPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Accueil'),
      ),
      body: Column(children: [
        UIButton(
          text: 'Change preferences',
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => PreferencesScreen(
                  title: 'Preferences',
                ),
              ),
            );
          },
        ),
        Text('Welcome homeScreeen')
      ]),
    );
  }
}
