import 'package:app/models/colors.dart';
import 'package:app/models/game_history.dart';
import 'package:app/models/move_played_return.dart';
import 'package:app/providers/sql_helper.dart';
import 'package:app/ui/screens/preferences_screen.dart';
import 'package:app/ui/widgets/history_card.dart';
import 'package:app/ui/widgets/history_filter.dart';
import 'package:flutter/material.dart';

class HistoryScreen extends StatefulWidget {
  HistoryScreen({super.key});

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  List<GameHistory> _games = [];
  bool _isLoading = false;
  bool showWin = true;
  bool showLoose = true;
  bool showDraw = true;
  bool showBlack = true;
  bool showWhite = true;

  Future<void> fetchGames() async {
    List<String> excludedResults = [];
    if (!showWin) excludedResults.add(VICTORY_LABEL);
    if (!showLoose) excludedResults.add(LOOSE_LABEL);
    if (!showDraw) excludedResults.add(DRAW_LABEL);

    List<String> excludedColors = [];
    if (!showBlack) excludedColors.add(BLACK_LABEL);
    if (!showWhite) excludedColors.add(WHITE_LABEL);
    final data =
        await SQLHelper.getGamesHistory(excludedResults, excludedColors);
    setState(() {
      _games = data;
      _isLoading = false;
    });
  }

  void deleteGame(int id) async {
    await SQLHelper.deleteGame(id);
    setState(() {
      _isLoading = true;
    });
    fetchGames();
  }

  @override
  void initState() {
    super.initState();
    fetchGames();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.settings_outlined),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => PreferencesScreen(
                  title: 'Preferences',
                ),
              ),
            );
          },
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: const Text(
          'Historique des parties',
          style:
              TextStyle(fontWeight: FontWeight.w900, fontFamily: 'Readex Pro'),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            child: Wrap(
              alignment: WrapAlignment.center,
              spacing: 15,
              children: [
                HistoryFilter(
                  text: 'Victoires',
                  onPressed: () => {
                    setState(() {
                      showWin = !showWin;
                      fetchGames();
                    })
                  },
                  enabled: showWin,
                ),
                HistoryFilter(
                  text: 'Défaites',
                  onPressed: () => {
                    setState(() {
                      showLoose = !showLoose;
                      fetchGames();
                    })
                  },
                  enabled: showLoose,
                ),
                HistoryFilter(
                  text: 'Nulles',
                  onPressed: () => {
                    setState(() {
                      showDraw = !showDraw;
                      fetchGames();
                    })
                  },
                  enabled: showDraw,
                ),
                HistoryFilter(
                  text: 'Blancs',
                  onPressed: () => {
                    setState(() {
                      showWhite = !showWhite;
                      fetchGames();
                    })
                  },
                  enabled: showWhite,
                ),
                HistoryFilter(
                  text: 'Noirs',
                  onPressed: () => {
                    setState(() {
                      showBlack = !showBlack;
                      fetchGames();
                    })
                  },
                  enabled: showBlack,
                ),
              ],
            ),
          ),
          _isLoading
              ? CircularProgressIndicator(
                  color: Theme.of(context).colorScheme.secondary,
                )
              : Expanded(
                  child: _games.length > 0
                      ? ListView.builder(
                          itemCount: _games.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 2),
                                child: HistoryCard(
                                  gameHistory: _games[index],
                                  onDelete: deleteGame,
                                ));
                          },
                        )
                      : Container(
                          padding: EdgeInsets.all(14),
                          child: Text(
                              'Vous n\'avez pas encore de partie dans votre historique')),
                ),
        ],
      ),
    );
  }
}
