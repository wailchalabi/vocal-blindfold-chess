import 'package:app/config/theme/color_theme.dart';
import 'package:app/providers/game.dart';
import 'package:app/providers/stockfish.dart';
import 'package:app/ui/screens/blind_game_screen.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:app/ui/widgets/current_game_status.dart';
import 'package:app/ui/widgets/dropdown_select.dart';
import 'package:app/ui/widgets/speech_to_text_input.dart';
import 'package:app/ui/widgets/stockfish_status.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockfish/stockfish.dart';

class PreferencesScreen extends StatefulWidget {
  PreferencesScreen({super.key, required this.title});
  final String title;

  @override
  State<PreferencesScreen> createState() => _PreferencesScreenState();
}

class _PreferencesScreenState extends State<PreferencesScreen> {
  late SharedPreferences prefs;

  int? selectedOption = null;
  bool soundOn = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _loadPrefs();
    });
  }

  _loadPrefs() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      selectedOption = prefs.getInt('stockfish') ?? 1;
      soundOn = prefs.getBool('soundOn') ?? true;
    });
  }

  void handleOptionChanged<T>(T? newOption) {
    setState(() {
      selectedOption = newOption as int;
    });
    prefs.setInt('stockfish', selectedOption ?? 1);
  }

  void handleSoundChange(bool newVal) {
    setState(() {
      soundOn = newVal;
    });
    prefs.setBool('soundOn', newVal);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Préférences',
          style:
              TextStyle(fontWeight: FontWeight.w900, fontFamily: 'Readex Pro'),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(24),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
            child: const Text('Niveau de l\'ordinateur',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Readex Pro')),
          ),
          Container(
            child: Expanded(
              child: GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemCount: 10,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 5, // number of columns in the grid
                  crossAxisSpacing: 12,
                  mainAxisSpacing: 12,
                ),
                itemBuilder: (BuildContext context, int index) {
                  return UIButton(
                    text: '${index + 1}',
                    onPressed: () {
                      handleOptionChanged(index + 1);
                    },
                    ph: 0,
                    elevation: index + 1 == selectedOption ? 3 : 0,
                    background: index + 1 == selectedOption
                        ? Theme.of(context).colorScheme.primary
                        : THEME_MIDGRAY,
                  );
                },
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
            child: const Text('Activer le son',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Readex Pro')),
          ),
          Container(
            child: Expanded(
              child: GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemCount: 2,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 5, // number of columns in the grid
                  crossAxisSpacing: 12,
                  mainAxisSpacing: 12,
                ),
                itemBuilder: (BuildContext context, int index) {
                  return UIButton(
                    text: index == 0 ? 'Oui' : 'Non',
                    onPressed: () {
                      handleSoundChange(index == 0 ? true : false);
                    },
                    ph: 0,
                    elevation: (index == 0 && soundOn == true) ||
                            (index == 1 && soundOn == false)
                        ? 3
                        : 0,
                    background: (index == 0 && soundOn == true) ||
                            (index == 1 && soundOn == false)
                        ? Theme.of(context).colorScheme.primary
                        : THEME_MIDGRAY,
                  );
                },
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
