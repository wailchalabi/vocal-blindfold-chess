import 'package:app/config/theme/color_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HistoryFilter extends StatelessWidget {
  final String text;
  final bool enabled;
  final VoidCallback onPressed;

  const HistoryFilter(
      {super.key,
      required this.text,
      required this.onPressed,
      required this.enabled});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: this.onPressed,
      child: Text(
        this.text,
        style: TextStyle(color: enabled ? THEME_LIGHT : Colors.black),
      ),
      style: ButtonStyle(
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
                40.0), // Adjust the value as per your preference
          ),
        ),
        elevation: MaterialStateProperty.all<double>(enabled ? 2 : 0),
        backgroundColor: MaterialStateProperty.resolveWith<Color?>(
          (Set<MaterialState> states) {
            if (enabled) {
              return THEME_SECONDARY;
            } else {
              return THEME_MIDGRAY;
            }
          },
        ),
      ),
    );
  }
}
