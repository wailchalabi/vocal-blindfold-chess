import 'package:app/config/theme/color_theme.dart';
import 'package:bubble_showcase/bubble_showcase.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

typedef void BoolCallback(bool? value);

class UiSwitch extends StatefulWidget {
  final BoolCallback onToggle;

  const UiSwitch({super.key, required this.onToggle});

  @override
  State<UiSwitch> createState() => _UiSwitchState();
}

class _UiSwitchState extends State<UiSwitch> {
  bool light = false;

  @override
  Widget build(BuildContext context) {
    return BubbleShowcase(
      bubbleShowcaseId: 'my_bubble_showcase',
      bubbleShowcaseVersion: 1,
      bubbleSlides: [
        AbsoluteBubbleSlide(
          positionCalculator: (size) => Position(
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
          ),
          child: AbsoluteBubbleSlideChild(
              positionCalculator: (size) => Position(
                    top: 0,
                    left: 0,
                  ),
              widget: Text('test')),
        ),
      ],
      child: Text('text2'),
    );
  }
}
