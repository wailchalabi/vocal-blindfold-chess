import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class UIButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final bool? disabled;
  final Icon? icon;
  final Color? background;
  final AssetImage? image;
  final double? elevation;
  final double? ph;
  final Color? textColor;
  final double? fontSize;

  const UIButton(
      {super.key,
      required this.text,
      required this.onPressed,
      this.disabled,
      this.icon,
      this.image,
      this.background,
      this.elevation,
      this.ph,
      this.fontSize,
      this.textColor});

  ButtonStyle buttonStyle(BuildContext context, {bool? disabled}) {
    return ButtonStyle(
      elevation: MaterialStateProperty.all<double>(elevation ?? 3),
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(60))),
      textStyle: MaterialStateProperty.all<TextStyle>(
          TextStyle(fontFamily: 'Readex Pro', fontSize: fontSize ?? 20)),
      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(EdgeInsets.only(
          top: 12,
          bottom: 12,
          right: ph ?? 48,
          left: text == '' ? (ph != null ? (ph! + 4) : 52) : ph ?? 48)),
      backgroundColor: MaterialStateProperty.resolveWith<Color?>(
        (Set<MaterialState> states) {
          if (background != null) {
            return background;
          } else if (disabled == true) {
            return Theme.of(context).colorScheme.tertiary;
          }
          return Theme.of(context).colorScheme.primary;
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (image != null) {
      return ElevatedButton.icon(
          onPressed: disabled == true ? null : this.onPressed,
          icon: Image(
            image: image!,
            width: 24,
            height: 24,
          ), //icon data for elevated button
          label: Text(this.text,
              style: TextStyle(color: textColor ?? Colors.black)), //label text
          style: buttonStyle(context, disabled: disabled));
    } else if (icon != null) {
      return ElevatedButton.icon(
          onPressed: disabled == true ? null : this.onPressed,
          icon: icon ?? Icon(Icons.save), //icon data for elevated button
          label: Text(this.text,
              style: TextStyle(color: textColor ?? Colors.black)), //label text
          style: buttonStyle(context, disabled: disabled));
    } else {
      return ElevatedButton(
          onPressed: disabled == true ? null : this.onPressed,
          child: Text(this.text,
              style: TextStyle(color: textColor ?? Colors.black)),
          style: buttonStyle(context, disabled: disabled));
    }
  }
}
