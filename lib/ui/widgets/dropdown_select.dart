import 'package:flutter/material.dart';

class DropdownOption<T> {
  final String label;
  final T value;

  DropdownOption({required this.label, required this.value});
}

class DropdownSelect<T> extends StatefulWidget {
  final List<DropdownOption<T>> options;
  final T selectedOption;
  final ValueChanged<T?> onChanged;
  final String label;

  DropdownSelect({
    required this.options,
    required this.selectedOption,
    required this.onChanged,
    required this.label,
  });

  @override
  _DropdownSelectState<T> createState() => _DropdownSelectState<T>();
}

class _DropdownSelectState<T> extends State<DropdownSelect<T>> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(widget.label),
        SizedBox(height: 8),
        DropdownButton<T>(
          value: widget.selectedOption,
          onChanged: widget.onChanged,
          items: widget.options.map((option) {
            return DropdownMenuItem<T>(
              value: option.value,
              child: Text(option.label),
            );
          }).toList(),
        ),
      ],
    );
  }
}
