import 'package:app/models/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chess_board/flutter_chess_board.dart';

class ColorButton extends StatelessWidget {
  final PlayerColor color;
  final VoidCallback onPressed;

  const ColorButton({Key? key, required this.color, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: onPressed,
        child: Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: color == PlayerColor.white ? Colors.white : Colors.black,
              borderRadius: BorderRadius.circular(10.0),
              boxShadow: [
                BoxShadow(
                    blurStyle: BlurStyle.outer,
                    color: Colors.grey.shade600,
                    spreadRadius: 0.2,
                    blurRadius: 1)
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ClipRRect(
                child: Image.asset(
                  color == PlayerColor.white
                      ? 'assets/king.png'
                      : 'assets/king_disabled.png',
                  fit: BoxFit.cover,
                  height: 48,
                  width: 48,
                ),
              ),
              Text(
                getColorLabel(color),
                style: TextStyle(
                    color: color == PlayerColor.black
                        ? Colors.white
                        : Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Readex Pro'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
