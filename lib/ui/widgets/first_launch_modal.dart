import 'package:app/config/theme/color_theme.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:flutter/material.dart';

class FirstLaunchModal extends StatefulWidget {
  @override
  _FirstLaunchModalState createState() => _FirstLaunchModalState();
}

class _FirstLaunchModalState extends State<FirstLaunchModal> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(36)),
      actionsAlignment: MainAxisAlignment.start,
      titlePadding: EdgeInsets.zero,
      buttonPadding: EdgeInsets.all(16),
      alignment: Alignment.center,
      titleTextStyle: const TextStyle(
          fontFamily: 'Readex Pro',
          color: Colors.black,
          fontSize: 20,
          fontWeight: FontWeight.w900),
      contentPadding: EdgeInsets.zero,
      title: Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(36), topRight: Radius.circular(36)),
          color: THEME_SECONDARY,
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image(image: AssetImage('assets/star.png'), height: 56),
                UIButton(
                    text: '',
                    ph: 0,
                    background: Colors.transparent,
                    elevation: 0,
                    icon: Icon(
                      Icons.close_rounded,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    })
              ],
            ),
            SizedBox(
              height: 24,
            ),
            const Text(
              'Bienvenue',
              style: const TextStyle(
                fontFamily: 'Readex Pro',
                fontWeight: FontWeight.w500,
                color: THEME_LIGHT,
                fontSize: 24,
              ),
            ),
          ],
        ),
      ),
      content: Container(
        padding: EdgeInsets.all(16),
        child: Column(children: [
          Text(
            'Merci d\'avoir téléchargé l\'application. ',
            style: TextStyle(
              color: THEME_DARKGRAY,
              fontSize: 18,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
              style: TextStyle(color: THEME_DARKGRAY),
              'Développée par loisir, le but de cette dernière est de permettre au joueur de s\'entrainer à jouer à l\'aveugle sans visualiser l\'échiquier. '),
          SizedBox(
            height: 8,
          ),
          Text(
              style: TextStyle(color: THEME_DARKGRAY),
              'Pour accompagner votre progression, vous pourrez ponctuellement afficher l\'échiquier au départ, ou questionner l\'application sur la positionnement de certaines pièces. Vous pourrez également augmenter progressivement la difficulté de l\'ordinateur.')
        ]),
      ),
      actions: [
        const Divider(
          color: THEME_MIDGRAY,
          thickness: 1.5,
          height: 0,
          indent: 10,
          endIndent: 10,
        ),
        Container(
          margin: const EdgeInsets.only(top: 24),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: UIButton(
                  background: THEME_SECONDARY,
                  textColor: THEME_LIGHT,
                  fontSize: 18,
                  ph: 12,
                  text: 'Aller jouer une partie',
                  onPressed: () {
                    Navigator.of(context)
                        .pop(true); // Return false when cancel is pressed
                  },
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
