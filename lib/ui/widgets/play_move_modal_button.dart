import 'package:app/config/theme/color_theme.dart';
import 'package:app/models/commands.dart';
import 'package:app/models/pieces.dart';
import 'package:app/providers/game.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:app/ui/widgets/piece_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

typedef void StringCallback(String string);

class PlayMoveModalButton extends StatefulWidget {
  const PlayMoveModalButton(
      {super.key,
      required this.context,
      required this.game,
      required this.onPlayMove});
  final Game game;
  final BuildContext context;
  final StringCallback onPlayMove;

  @override
  State<PlayMoveModalButton> createState() => _PlayMoveModalButtonState();
}

class _PlayMoveModalButtonState extends State<PlayMoveModalButton> {
  @override
  void initState() {
    super.initState();
  }

  PieceInfos? _selectedPiece;
  String? _selectedMove;

  void onClickTile() {
    Navigator.of(context).pop();
    if (_selectedMove != null) {
      widget.onPlayMove(_selectedMove!);
    }
  }

  void handleClickPiece() {
    _selectedMove = null;
    List<String> moves = [];
    widget.game.chess.moves().forEach(
          (element) => {
            if (_selectedPiece == PAWN &&
                ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'].contains(element[0]))
              {moves.add(element)}
            else if (element[0] == _selectedPiece?.move)
              {moves.add(element)}
          },
        );
    if (_selectedPiece == ROOK || _selectedPiece == KING) {
      if (widget.game.chess.moves().contains(SHORT_CASTLE_MOVE)) {
        moves.add(SHORT_CASTLE_MOVE);
      }
      if (widget.game.chess.moves().contains(LONG_CASTLE_MOVE)) {
        moves.add(LONG_CASTLE_MOVE);
      }
    }

    Navigator.of(context).pop();

    showModalBottomSheet(
      context: context,
      barrierColor: Colors.white.withOpacity(0.3),
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return Container(
          height: (moves.length / 3).abs().ceil() * 77,
          margin: const EdgeInsets.only(left: 30, right: 30, bottom: 30),
          padding: const EdgeInsets.all(6.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12.0),
              boxShadow: [
                BoxShadow(
                    blurStyle: BlurStyle.outer,
                    color: Colors.grey.shade600,
                    spreadRadius: 1,
                    blurRadius: 7)
              ]),
          child: GridView.builder(
            itemCount: moves.length,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, // number of columns in the grid
              crossAxisSpacing: 6,
              mainAxisSpacing: 6,
              childAspectRatio: 1.565,
            ),
            itemBuilder: (BuildContext context, int index) {
              return PieceButton(
                  pieceName: moves[index],
                  disabled: false,
                  onPressed: () {
                    setState(() {
                      _selectedMove = moves[index];
                    });
                    onClickTile();
                  },
                  icon: _selectedPiece!.icon);
            },
          ),
        );
      },
    ).whenComplete(() => {
          if (_selectedMove == null) {handlePress()}
        });
  }

  void handlePress() {
    _selectedMove = null;
    showModalBottomSheet(
      context: context,
      barrierColor: Colors.white.withOpacity(0.3),
      backgroundColor: Colors.transparent,
      builder: (BuildContext ctx) {
        return Container(
          height: 160,
          margin: const EdgeInsets.only(left: 30, right: 30, bottom: 30),
          padding: const EdgeInsets.all(6.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12.0),
              boxShadow: [
                BoxShadow(
                    blurStyle: BlurStyle.outer,
                    color: Colors.grey.shade600,
                    spreadRadius: 1,
                    blurRadius: 7)
              ]),
          child: GridView.builder(
            itemCount: 6,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, // number of columns in the grid
              crossAxisSpacing: 6,
              mainAxisSpacing: 6,
              childAspectRatio: 1.50,
            ),
            itemBuilder: (BuildContext context, int index) {
              bool hasMoves = widget.game.chess
                  .moves()
                  .any((element) => element.contains(PiecesList[index].move));
              return PieceButton(
                  pieceName: PiecesList[index].label,
                  disabled: !hasMoves,
                  onPressed: () {
                    setState(() {
                      _selectedPiece = PiecesList[index];
                    });
                    handleClickPiece();
                  },
                  icon: hasMoves
                      ? PiecesList[index].icon
                      : PiecesList[index].iconDisabled);
            },
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return UIButton(
      ph: 36,
      elevation: 0,
      disabled: Provider.of<Game>(context).iaTurn,
      background: THEME_LIGHT,
      onPressed: handlePress,
      text: 'Manuel',
      image: AssetImage('assets/keyboard.png'),
    );
  }
}
