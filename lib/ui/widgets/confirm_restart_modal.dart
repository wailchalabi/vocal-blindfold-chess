import 'package:app/config/theme/color_theme.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:flutter/material.dart';

class ConfirmRestartModal extends StatefulWidget {
  @override
  _ConfirmRestartModalState createState() => _ConfirmRestartModalState();
}

class _ConfirmRestartModalState extends State<ConfirmRestartModal> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(36)),
      actionsAlignment: MainAxisAlignment.start,
      titlePadding: EdgeInsets.zero,
      buttonPadding: EdgeInsets.all(16),
      alignment: Alignment.center,
      titleTextStyle: const TextStyle(
          fontFamily: 'Readex Pro',
          color: Colors.black,
          fontSize: 20,
          fontWeight: FontWeight.w900),
      contentPadding: EdgeInsets.zero,
      title: Container(
        padding: EdgeInsets.all(24),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(36), topRight: Radius.circular(36)),
          color: THEME_SECONDARY,
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image(image: AssetImage('assets/warning.png'), height: 56),
                UIButton(
                    text: '',
                    ph: 0,
                    background: Colors.transparent,
                    elevation: 0,
                    icon: Icon(
                      Icons.close_rounded,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    })
              ],
            ),
            SizedBox(
              height: 24,
            ),
            const Text(
              'Voulez vous quitter la partie en cours de jeu ?',
              style: const TextStyle(
                fontFamily: 'Readex Pro',
                fontWeight: FontWeight.w500,
                color: THEME_LIGHT,
                fontSize: 24,
              ),
            ),
          ],
        ),
      ),
      content: Container(
        padding: EdgeInsets.all(24),
        child: Text(
          'Si vous quittez la partie en cours, vous perdrez toute votre progression. Êtes-vous sûr de vouloir quitter ?',
          style: TextStyle(color: THEME_DARKGRAY),
        ),
      ),
      actions: [
        const Divider(
          color: THEME_MIDGRAY,
          thickness: 1.5,
          height: 0,
          indent: 10,
          endIndent: 10,
        ),
        Container(
          margin: const EdgeInsets.only(top: 24),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: UIButton(
                  ph: 12,
                  text: 'Quitter',
                  fontSize: 16,
                  background: THEME_RED,
                  textColor: THEME_LIGHT,
                  onPressed: () {
                    Navigator.of(context)
                        .pop(true); // Return true when proceed is pressed
                  },
                ),
              ),
              const SizedBox(width: 20),
              Expanded(
                child: UIButton(
                  background: THEME_SECONDARY,
                  textColor: THEME_LIGHT,
                  fontSize: 16,
                  ph: 12,
                  text: 'Continuer',
                  onPressed: () {
                    Navigator.of(context)
                        .pop(false); // Return false when cancel is pressed
                  },
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
