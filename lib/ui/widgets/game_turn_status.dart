import 'package:app/providers/game.dart';
import 'package:app/providers/stockfish.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:app/ui/widgets/switch.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:stockfish/stockfish.dart';

class GameTurnStatus extends StatelessWidget {
  const GameTurnStatus({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
              Provider.of<Game>(context).iaTurn
                  ? 'Tour de l\'IA'
                  : 'Votre tour',
              style: TextStyle(
                  fontWeight: FontWeight.w900, fontFamily: 'Readex Pro')),
          SizedBox(width: 20),
          Visibility(
            child: CircularProgressIndicator(
              color: Theme.of(context).colorScheme.secondary,
            ),
            visible: Provider.of<Game>(context).iaTurn,
          ),
        ],
      ),
    );
  }
}
