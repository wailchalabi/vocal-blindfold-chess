import 'package:app/config/theme/color_theme.dart';
import 'package:app/providers/game.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:stockfish/stockfish.dart';

class CurrentGameStatus extends StatelessWidget {
  final bool loading;

  const CurrentGameStatus({
    required this.loading,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    String text = Provider.of<Game>(context).currentPgn;
    RegExp regExp = RegExp(r'\d+\.');

    List<TextSpan> spans = [];

    if (Provider.of<Game>(context).gameOn == false) {
      spans.add(
        TextSpan(
            text: 'Commencez une partie à l\'aveugle.',
            style: TextStyle(color: Colors.black, fontSize: 15)),
      );
    } else if (text == '1.') {
      spans.add(
        TextSpan(
            text: 'Jouez un coup pour lancer la partie',
            style: TextStyle(color: Colors.black, fontSize: 15)),
      );
    } else {
      int start = 0;
      for (RegExpMatch match in regExp.allMatches(text)) {
        spans.add(TextSpan(
            text: text.substring(
              start,
              match.start,
            ),
            style: TextStyle(color: Colors.grey)));

        spans.add(TextSpan(
          text: text.substring(match.start, match.end),
          style: TextStyle(color: Colors.black),
        ));

        start = match.end;
      }

      spans.add(
        TextSpan(
            text: text.substring(start), style: TextStyle(color: Colors.grey)),
      );
    }

    RichText richText = RichText(text: TextSpan(children: spans));

    return Container(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 12),
            padding: EdgeInsets.all(24),
            decoration: BoxDecoration(
              color: THEME_LIGHT,
              borderRadius: BorderRadius.circular(12.0),
            ),
            constraints: BoxConstraints(minHeight: 100),
            width: MediaQuery.of(context).size.width * 0.8,
            child: loading
                ? Container(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  )
                : richText,
          ),
        ],
      ),
    );
  }
}
