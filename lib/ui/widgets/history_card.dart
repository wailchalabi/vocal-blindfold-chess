import 'package:app/config/theme/color_theme.dart';
import 'package:app/models/colors.dart';
import 'package:app/models/game_history.dart';
import 'package:app/models/move_played_return.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:app/utils/date.dart';
import 'package:flutter/material.dart';

typedef void IntCallback(int int);

class HistoryCard extends StatelessWidget {
  final GameHistory gameHistory;
  final IntCallback onDelete;

  const HistoryCard({
    Key? key,
    required this.gameHistory,
    required this.onDelete,
  }) : super(key: key);

  void openLichess(BuildContext context) {
    openAnalysisBoard(
        this.gameHistory.pgn, getLabelColor(this.gameHistory.color), context);
  }

  Color getCircleColor() {
    switch (this.gameHistory.result) {
      case 'Victoire':
        return Colors.green;
      case 'Défaite':
        return Colors.red;
    }
    return Colors.grey;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Card(
        color: THEME_MIDGRAY, // color is now set to blue
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: ExpansionTile(
            childrenPadding: EdgeInsets.only(left: 8, bottom: 14),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CircleAvatar(
                  radius: 10, // Adjust the radius as per your requirement
                  backgroundColor: getCircleColor(), // Set the desired color
                ),
                const SizedBox(
                  width: 15,
                ),
                Text(
                  "${gameHistory.moves} coups avec les ${gameHistory.color}",
                  style: const TextStyle(color: Colors.black),
                ),
              ],
            ),
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width - 120,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Le ${formatDate(gameHistory.createdAt)}"),
                            SizedBox(
                              height: 8,
                            ),
                            Text("Ordinateur niveau ${gameHistory.difficulty}"),
                            SizedBox(
                              height: 8,
                            ),
                            Text(gameHistory.pgn),
                          ],
                        ),
                      ),
                      UIButton(
                        text: '',
                        onPressed: () => onDelete(gameHistory.id),
                        image: const AssetImage('assets/delete.png'),
                        ph: 0,
                        background: Colors.transparent,
                        elevation: 0,
                        fontSize: 14,
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 8),
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: UIButton(
                          text: 'Analyser',
                          elevation: 0,
                          onPressed: () => openLichess(context),
                          image: const AssetImage('assets/board.png'),
                          fontSize: 14,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
