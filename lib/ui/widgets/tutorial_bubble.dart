import 'package:bubble_showcase/bubble_showcase.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:speech_bubble/speech_bubble.dart';

typedef void BoolCallback(bool? value);

class TutorialBubble extends StatefulWidget {
  const TutorialBubble({super.key, required});

  @override
  State<TutorialBubble> createState() => _TutorialBubbleState();
}

class _TutorialBubbleState extends State<TutorialBubble> {
  bool light = false;

  @override
  Widget build(BuildContext context) {
    return BubbleShowcase(
      doNotReopenOnClose: true,
      bubbleShowcaseId: 'my_bubble_showcase',
      bubbleShowcaseVersion: 1,
      showCloseButton: false,
      bubbleSlides: [
        AbsoluteBubbleSlide(
          positionCalculator: (size) => Position(
            top: 30,
            right: MediaQuery.of(context).size.width - 3,
            bottom: 85,
          ),
          shape: const RoundedRectangle(radius: Radius.circular(45)),
          child: AbsoluteBubbleSlideChild(
            positionCalculator: (size) => const Position(
              top: 100,
              left: 30,
            ),
            widget: SpeechBubble(
              nipLocation: NipLocation.TOP_LEFT,
              color: Colors.blue,
              child: const Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Règlez le son ou le niveau de difficulté ici',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ),
        AbsoluteBubbleSlide(
          positionCalculator: (size) => Position(
            top: 30,
            right: MediaQuery.of(context).size.width - 3,
            bottom: 85,
          ),
          shape: const RoundedRectangle(radius: Radius.circular(45)),
          child: AbsoluteBubbleSlideChild(
            positionCalculator: (size) => const Position(top: 100, right: 10),
            widget: SpeechBubble(
              nipLocation: NipLocation.TOP_RIGHT,
              color: Colors.blue,
              child: const Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Recommencez votre partie ici',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ),
        AbsoluteBubbleSlide(
          positionCalculator: (size) => Position(
            top: MediaQuery.of(context).size.height - 240,
            right: MediaQuery.of(context).size.width - 3,
            bottom: MediaQuery.of(context).size.height - 150,
          ),
          shape: const RoundedRectangle(radius: Radius.circular(45)),
          child: AbsoluteBubbleSlideChild(
            positionCalculator: (size) => Position(bottom: 250, left: 50),
            widget: SpeechBubble(
              nipLocation: NipLocation.BOTTOM_LEFT,
              color: Colors.blue,
              child: const Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Vous pouvez jouer un coup manuellement parmis la liste des coups légaux',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ),
        AbsoluteBubbleSlide(
          positionCalculator: (size) => Position(
            top: MediaQuery.of(context).size.height - 240,
            right: MediaQuery.of(context).size.width - 3,
            bottom: MediaQuery.of(context).size.height - 150,
          ),
          shape: const RoundedRectangle(radius: Radius.circular(45)),
          child: AbsoluteBubbleSlideChild(
            positionCalculator: (size) => Position(bottom: 250, right: 50),
            widget: SpeechBubble(
              nipLocation: NipLocation.BOTTOM_RIGHT,
              color: Colors.blue,
              child: const Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Ou annoncer vocalement les coups à jouer. Pour cela, annoncez la case de départ et d\'arrivée de la pièce. Ex: e2 e4',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ),
        AbsoluteBubbleSlide(
          positionCalculator: (size) => Position(
            top: MediaQuery.of(context).size.height - 160,
            right: MediaQuery.of(context).size.width - 3,
            bottom: MediaQuery.of(context).size.height - 75,
          ),
          shape: const RoundedRectangle(radius: Radius.circular(45)),
          child: AbsoluteBubbleSlideChild(
            positionCalculator: (size) => Position(bottom: 170),
            widget: SpeechBubble(
              nipLocation: NipLocation.BOTTOM,
              color: Colors.blue,
              child: const Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Pour vous rememorer l\'échiquier, cliquez ici.',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ),
        AbsoluteBubbleSlide(
          positionCalculator: (size) => Position(
            top: MediaQuery.of(context).size.height - 65,
            left: 40,
            right: MediaQuery.of(context).size.width - 40,
            bottom: MediaQuery.of(context).size.height - 15,
          ),
          shape: const RoundedRectangle(radius: Radius.circular(45)),
          child: AbsoluteBubbleSlideChild(
            positionCalculator: (size) => Position(
                bottom: 80, right: MediaQuery.of(context).size.width * 0.35),
            widget: SpeechBubble(
              nipLocation: NipLocation.BOTTOM,
              color: Colors.blue,
              child: const Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Onglet de jeu',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ),
        AbsoluteBubbleSlide(
          positionCalculator: (size) => Position(
            top: MediaQuery.of(context).size.height - 65,
            left: 40,
            right: MediaQuery.of(context).size.width - 40,
            bottom: MediaQuery.of(context).size.height - 15,
          ),
          shape: const RoundedRectangle(radius: Radius.circular(45)),
          child: AbsoluteBubbleSlideChild(
            positionCalculator: (size) => Position(
                bottom: 80, left: MediaQuery.of(context).size.width * 0.35),
            widget: SpeechBubble(
              nipLocation: NipLocation.BOTTOM,
              color: Colors.blue,
              child: const Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Onglet d\'historique',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ),
      ],
      child: const Text(''),
    );
  }
}
