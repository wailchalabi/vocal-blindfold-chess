import 'package:app/providers/stockfish.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:app/ui/widgets/switch.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:stockfish/stockfish.dart';

typedef void BoolCallback(bool? value);

class StockfishStatus extends StatelessWidget {
  final BoolCallback onToggle;

  const StockfishStatus({
    super.key,
    required this.onToggle,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Visibility(
            child: CircularProgressIndicator(
              color: Theme.of(context).colorScheme.secondary,
            ),
            visible: Provider.of<StockfishAI>(context).loading,
          ),
          UiSwitch(onToggle: onToggle),
        ],
      ),
    );
  }
}
