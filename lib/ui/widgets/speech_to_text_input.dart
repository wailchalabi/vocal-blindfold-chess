import 'package:app/config/theme/color_theme.dart';
import 'package:app/models/commands.dart';
import 'package:app/providers/game.dart';
import 'package:app/providers/stockfish.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';

typedef void StringCallback(String string);

class SpeakModal extends StatefulWidget {
  final StringCallback onSpeechChange;

  SpeakModal({Key? key, required this.onSpeechChange}) : super(key: key);
  @override
  _SpeakModalState createState() => _SpeakModalState();
}

class _SpeakModalState extends State<SpeakModal> {
  late SharedPreferences prefs;
  SpeechToText _speechToText = SpeechToText();
  bool _speechEnabled = false;
  bool isListening = false;

  @override
  void initState() {
    super.initState();
    _initSpeech();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _loadPrefs();
    });
  }

  _loadPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  /// This has to happen only once per app
  void _initSpeech() async {
    _speechEnabled = await _speechToText.initialize();
  }

  /// Each time to start a speech recognition session

  /// This is the callback that the SpeechToText plugin calls when
  /// the platform returns recognized words.
  void _onSpeechResult(SpeechRecognitionResult result) {
    if (result.finalResult) {
      String allWords = result.alternates
          .map((alt) => alt.recognizedWords)
          .toList()
          .join(" ");
      widget.onSpeechChange(allWords);
    }
  }

  void _startTalking() async {
    if (!isListening) {
      await _speechToText.listen(onResult: _onSpeechResult, localeId: 'fr_FR');
      setState(() {
        isListening = true;
      });
      await Future.delayed(const Duration(milliseconds: 4000));
      _speechToText.stop();
      setState(() {
        isListening = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 30, right: 30, bottom: 10),
      padding: const EdgeInsets.all(4.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12.0),
          boxShadow: [
            BoxShadow(
                blurStyle: BlurStyle.outer,
                color: Colors.grey.shade600,
                spreadRadius: 1,
                blurRadius: 7)
          ]),
      child: Container(
        padding: const EdgeInsets.all(12),
        child: Column(children: [
          Padding(
            padding: EdgeInsets.only(bottom: 12),
            child: UIButton(
              icon: Icon(
                isListening ? Icons.mic_outlined : Icons.mic_off_outlined,
                color: isListening ? THEME_RED : Colors.black,
              ),
              text: isListening ? 'Annoncez votre coup' : 'Appuyez pour parler',
              onPressed: _startTalking,
              disabled: isListening,
              ph: 16,
              textColor: isListening ? THEME_RED : Colors.black,
              background: THEME_PRIMARY,
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                const Padding(
                  padding: EdgeInsets.all(14),
                  child: Divider(
                    color: Colors.grey,
                    thickness: 1,
                    height: 2,
                    indent: 10,
                    endIndent: 10,
                  ),
                ),
                const Text(
                  'Coups',
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Readex Pro'),
                ),
                const Text(
                  'Annoncez la case de départ et d\'arrivée de votre pièce',
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Readex Pro'),
                ),
                Container(
                    margin: const EdgeInsets.only(top: 10),
                    height: 125, // Define the desired height
                    child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: MovesExamples.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: const EdgeInsets.symmetric(vertical: 4),
                          child: Row(children: [
                            Container(
                                width: 115,
                                child: Text(MovesExamples[index].label,
                                    style: const TextStyle(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: 'Readex Pro'))),
                            Text(MovesExamples[index].move,
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Readex Pro'))
                          ]),
                        );
                      },
                    )),
                const Text(
                  'Questions',
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Readex Pro'),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10),
                  height: 80, // Define the desired height
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: CommandsExemples.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        margin: const EdgeInsets.symmetric(vertical: 4),
                        child: Row(children: [
                          Container(
                            width: 115,
                            child: Text(
                              CommandsExemples[index].label,
                              style: const TextStyle(
                                  color: Colors.grey,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: 'Readex Pro'),
                            ),
                          ),
                          Text(
                            CommandsExemples[index].move,
                            style: const TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Readex Pro'),
                          )
                        ]),
                      );
                    },
                  ),
                ),
              ],
            ),
          )
        ]),
      ),
    );
  }
}

class SpeechToTextInput extends StatefulWidget {
  final StringCallback onSpeechChange;

  SpeechToTextInput({Key? key, required this.onSpeechChange}) : super(key: key);

  @override
  _SpeechToTextInputState createState() => _SpeechToTextInputState();
}

class _SpeechToTextInputState extends State<SpeechToTextInput> {
  void _openModal(BuildContext ctx) async {
    // ignore: use_build_context_synchronously
    showModalBottomSheet(
      context: ctx,
      barrierColor: Colors.white.withOpacity(0.3),
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return SpeakModal(onSpeechChange: widget.onSpeechChange);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return UIButton(
      elevation: 0,
      ph: 36,
      disabled: Provider.of<Game>(context).iaTurn,
      background: THEME_LIGHT,
      onPressed: () => _openModal(context),
      text: 'Vocal',
      image: const AssetImage('assets/micro.png'),
    );
  }
}
