import 'package:app/providers/game.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chess_board/flutter_chess_board.dart';
import 'package:provider/provider.dart';

class ChessboardModalButton extends StatefulWidget {
  const ChessboardModalButton({required this.context, required this.game});
  final Game game;
  final BuildContext context;

  @override
  State<ChessboardModalButton> createState() => _ChessboardModalButtonState();
}

class _ChessboardModalButtonState extends State<ChessboardModalButton> {
  ChessBoardController controller = ChessBoardController();

  @override
  void initState() {
    super.initState();
  }

  void handlePress() {
    controller.loadFen(widget.game.chess.fen);
    showModalBottomSheet(
      context: context,
      barrierColor: Colors.white.withOpacity(0.3),
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return Container(
          margin: const EdgeInsets.only(left: 30, right: 30, bottom: 30),
          padding: const EdgeInsets.all(6.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12.0),
              boxShadow: [
                BoxShadow(
                    blurStyle: BlurStyle.outer,
                    color: Colors.grey.shade600,
                    spreadRadius: 1,
                    blurRadius: 7)
              ]),
          child: Column(
            children: [
              ChessBoard(
                enableUserMoves: false,
                controller: controller,
                boardColor: BoardColor.brown,
                boardOrientation: widget.game.playerColor ?? PlayerColor.white,
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      child: UIButton(
        onPressed: handlePress,
        text: 'Afficher l\'échiquier',
        image: AssetImage('assets/board.png'),
      ),
    );
  }
}
