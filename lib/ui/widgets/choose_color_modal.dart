import 'package:app/models/colors.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:app/ui/widgets/color_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chess_board/flutter_chess_board.dart';

class ChooseColorModal extends StatefulWidget {
  @override
  _ChooseColorModalState createState() => _ChooseColorModalState();
}

class _ChooseColorModalState extends State<ChooseColorModal> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      actionsPadding: EdgeInsets.all(24),
      actionsAlignment: MainAxisAlignment.start,
      alignment: Alignment.center,
      titleTextStyle: const TextStyle(
          fontFamily: 'Readex Pro',
          color: Colors.black,
          fontSize: 20,
          fontWeight: FontWeight.w900),
      title: const Text('Choix de la couleur'),
      content: const Text(
        'Quelle couleur souhaitez vous jouer?',
        style: TextStyle(
          fontFamily: 'Readex Pro',
          color: Colors.black,
          fontSize: 16,
        ),
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: ColorButton(
                color: PlayerColor.white,
                onPressed: () {
                  Navigator.of(context).pop(
                      PlayerColor.white); // Return true when proceed is pressed
                },
              ),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: ColorButton(
                color: PlayerColor.black,
                onPressed: () {
                  Navigator.of(context).pop(
                      PlayerColor.black); // Return true when proceed is pressed
                },
              ),
            )
          ],
        )
      ],
    );
  }
}
