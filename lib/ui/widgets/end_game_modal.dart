import 'package:app/models/colors.dart';
import 'package:app/models/move_played_return.dart';
import 'package:app/ui/widgets/button.dart';
import 'package:app/ui/widgets/color_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chess_board/flutter_chess_board.dart';

class EndGameModal extends StatefulWidget {
  const EndGameModal({required this.res});
  final GameResult res;
  @override
  _EndGameModalState createState() => _EndGameModalState();
}

class _EndGameModalState extends State<EndGameModal> {
  String gameTitleByRes(GameResult res) {
    if (res == GameResult.WON) return 'Victoire';
    if (res == GameResult.LOOSE)
      return 'Défaite';
    else
      return 'Partie nulle';
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      actionsPadding: EdgeInsets.all(24),
      actionsAlignment: MainAxisAlignment.start,
      alignment: Alignment.center,
      titleTextStyle: const TextStyle(
          fontFamily: 'Readex Pro',
          color: Colors.black,
          fontSize: 20,
          fontWeight: FontWeight.w900),
      title: Text(this.gameTitleByRes(widget.res)),
      content: Text(
          'Bravo pour cette partie, vous pouvez l\'analyser sur Lichess si vous le souhaitez'),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: UIButton(
                text: 'Analyser la partie',
                onPressed: () {
                  Navigator.of(context)
                      .pop(true); // Return true when proceed is pressed
                },
              ),
            ),
          ],
        )
      ],
    );
  }
}
