import 'package:app/config/theme/color_theme.dart';
import 'package:flutter/material.dart';

class PieceButton extends StatelessWidget {
  final String pieceName;
  final String icon;
  final VoidCallback onPressed;
  final bool disabled;

  const PieceButton(
      {Key? key,
      required this.pieceName,
      required this.onPressed,
      required this.disabled,
      required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: disabled ? () => null : onPressed,
        child: Container(
          padding: const EdgeInsets.all(4),
          decoration: BoxDecoration(
            color: disabled ? THEME_GRAY_DISABLED : THEME_GRAY,
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ClipRRect(
                child: Image.asset(
                  icon,
                  fit: BoxFit.contain,
                  height: 33.5,
                  width: 33.5,
                ),
              ),
              Text(
                pieceName,
                style: TextStyle(
                  fontSize: 15,
                  color: disabled
                      ? const Color.fromARGB(255, 213, 214, 215)
                      : Color.fromARGB(255, 167, 170, 173),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
