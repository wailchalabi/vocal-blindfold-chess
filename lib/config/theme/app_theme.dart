import 'package:app/config/theme/color_theme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

// Instantiate new  theme data
final ThemeData appTheme = _appTheme();

//Define Base theme for app
ThemeData _appTheme() {
// We'll just overwrite whatever's already there using ThemeData.light()
  final ThemeData base = ThemeData.light();
  // Make changes to light() theme
  return base.copyWith(
    colorScheme: base.colorScheme.copyWith(
      primary: THEME_PRIMARY,
      onPrimary: Colors.black,
      secondary: THEME_SECONDARY,
      onSecondary: THEME_LIGHT,
      tertiary: THEME_RED,
      onTertiary: THEME_LIGHT,
      background: THEME_LIGHT,
      onBackground: Colors.black,
      shadow: THEME_SHADOW,
    ),
  );
}
