import 'package:flutter/widgets.dart';

const Color THEME_RED = Color(0xffFF3B30);
const Color THEME_LIGHT = Color(0xFFF4F5F6);
const Color THEME_MIDGRAY = Color(0xFFE9EAEB);
const Color THEME_DARKGRAY = Color.fromARGB(255, 122, 124, 126);
const Color THEME_GRAY = Color(0xFFF3F3F3);
const Color THEME_GRAY_DISABLED = Color(0xffFDFDFD);
const Color THEME_SHADOW = Color.fromRGBO(135, 135, 135, 1);
const Color THEME_PRIMARY = Color.fromRGBO(173, 236, 203, 1);
const Color THEME_SECONDARY = Color(0xFF4DAFFF);
