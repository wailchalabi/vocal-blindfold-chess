List<String> getChessTiles() {
  List<String> tiles = [];

  for (int row = 1; row <= 8; row++) {
    for (int col = 1; col <= 8; col++) {
      String tile = String.fromCharCode(col + 96) + row.toString();
      tiles.add(tile);
    }
  }

  return tiles;
}
