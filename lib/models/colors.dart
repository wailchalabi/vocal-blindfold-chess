import 'package:chess/chess.dart';
import 'package:flutter_chess_board/flutter_chess_board.dart';

const BLACK_LABEL = 'Noirs';
const WHITE_LABEL = 'Blancs';

String getColorLabel(PlayerColor color) {
  switch (color) {
    case PlayerColor.black:
      return BLACK_LABEL;
    case PlayerColor.white:
      return WHITE_LABEL;
  }
}

PlayerColor getLabelColor(String color) {
  switch (color) {
    case BLACK_LABEL:
      return PlayerColor.black;
    case WHITE_LABEL:
      return PlayerColor.white;
  }
  return PlayerColor.white;
}
