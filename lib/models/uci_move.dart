class UciMove {
  String from;
  String to;
  String? promotion;

  UciMove({required this.from, required this.to, this.promotion});

  Map<String, dynamic> toJson() =>
      {'from': from, 'to': to, 'promotion': promotion};

  String toString() => '${from}${to}${promotion != null ? promotion : ""}';
}

UciMove parseUCIMove(String uciMove) {
  String from = uciMove.substring(0, 2);
  String to = uciMove.substring(2, 4);
  String? promotion;

  if (uciMove.length == 5) {
    promotion = uciMove.substring(4);
  }

  UciMove move = UciMove(from: from, to: to);
  if (promotion != null) {
    move.promotion = promotion;
  }
  return move;
}
