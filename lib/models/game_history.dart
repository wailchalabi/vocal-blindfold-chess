class GameHistory {
  final int id;
  final int moves;
  final String pgn;
  final String result;
  final String color;
  final int difficulty;
  final String createdAt;

  GameHistory({
    required this.id,
    required this.moves,
    required this.pgn,
    required this.result,
    required this.color,
    required this.difficulty,
    required this.createdAt,
  });

  factory GameHistory.fromJson(Map<String, dynamic> json) {
    return GameHistory(
        id: json['id'] as int,
        moves: json['moves'] as int,
        difficulty: json['difficulty'] as int,
        pgn: json['pgn'] as String,
        result: json['result'] as String,
        color: json['color'] as String,
        createdAt: json['createdAt'] as String);
  }

  static List<GameHistory> fromJsonList(List<Map<String, dynamic>> jsonList) {
    return jsonList.map((json) => GameHistory.fromJson(json)).toList();
  }
}
