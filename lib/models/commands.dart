List<String> CommandsArray = [
  'prend',
  'petit roque',
  'grand roque',
  'case',
  'légal',
];

List<String> CaseCommand = ['casse', 'casier', 'cassel'];
List<String> LegalCommand = ['légale', 'égale', 'gale'];
List<String> CastleCommand = ['rock', 'roc'];

const SHORT_CASTLE_LABEL = 'Petit roque';
const LONG_CASTLE_LABEL = 'Grand roque';

const SHORT_CASTLE_MOVE = 'O-O';
const LONG_CASTLE_MOVE = 'O-O-O';

class ExampleMove {
  String label;
  String move;

  ExampleMove({
    required this.label,
    required this.move,
  }) {}
}

List<ExampleMove> MovesExamples = [
  ExampleMove(label: 'b1 c3', move: '1.Nc3'),
  ExampleMove(label: 'e7 e8 Dame', move: 'e8Q'),
  ExampleMove(label: 'e1 g1', move: 'O-O'),
  ExampleMove(label: 'e1 c1', move: 'O-O-O'),
];

List<ExampleMove> CommandsExemples = [
  ExampleMove(label: 'Case c3', move: 'Nom de pièce / Vide'),
  ExampleMove(label: 'Légal e2 c3', move: 'Légal / Illégal'),
];

String parseAmbiguousWords(String sentence) {
  String parsed = sentence;
  for (String word in CaseCommand) {
    parsed = parsed.replaceAll(word, 'case');
  }
  for (String word in LegalCommand) {
    parsed = parsed.replaceAll(word, 'légal');
  }
  for (String word in CastleCommand) {
    parsed = parsed.replaceAll(word, 'roque');
  }
  return parsed;
}
