import 'package:flutter/material.dart';
import 'package:flutter_chess_board/flutter_chess_board.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

String VICTORY_LABEL = 'Victoire';
String LOOSE_LABEL = 'Défaite';
String DRAW_LABEL = 'Partie nulle';
String NOT_FINISHED_LABEL = 'Pas terminée';

enum GameResult { WON, LOOSE, DRAW, NOT_FINISHED }

String gameResultString(GameResult value) {
  switch (value) {
    case GameResult.WON:
      return VICTORY_LABEL;
    case GameResult.LOOSE:
      return LOOSE_LABEL;
    case GameResult.DRAW:
      return DRAW_LABEL;
    case GameResult.NOT_FINISHED:
      return NOT_FINISHED_LABEL;
  }
}

class MovePlayedReturn {
  bool isLegal;
  GameResult? result;

  MovePlayedReturn({required this.isLegal, this.result});
}

void openAnalysisBoard(String pgn, PlayerColor color, BuildContext context) {
  final encodedPgn = Uri.encodeComponent(pgn);

  String inUrlColor = color == PlayerColor.black ? 'black' : 'white';

  final url =
      'https://lichess.org/analysis/pgn/$encodedPgn?color=${inUrlColor}';

  Navigator.of(context).push(
    MaterialPageRoute<void>(
      builder: (BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('Analyse de la partie avec Lichess'),
        ),
        body: InAppWebView(
          initialUrlRequest: URLRequest(url: Uri.parse(url)),
        ),
      ),
    ),
  );
}
