import 'package:chess/chess.dart';

List<PieceInfos> PiecesList = [
  KNIGHT,
  BISHOP,
  ROOK,
  QUEEN,
  KING,
  PAWN,
];

List<PieceInfos> PiecesListWithoutPawn = [
  KNIGHT,
  BISHOP,
  ROOK,
  QUEEN,
  KING,
];

const KNIGHT_LABEL = 'Cavalier';
const BISHOP_LABEL = 'Fou';
const QUEEN_LABEL = 'Dame';
const KING_LABEL = 'Roi';
const ROOK_LABEL = 'Tour';
const PAWN_LABEL = 'Pion';

List<String> PiecesLabel = [
  PAWN_LABEL,
  KNIGHT_LABEL,
  BISHOP_LABEL,
  ROOK_LABEL,
  QUEEN_LABEL,
  KING_LABEL,
];

const KNIGHT_MOVE = 'N';
const BISHOP_MOVE = 'B';
const QUEEN_MOVE = 'Q';
const KING_MOVE = 'K';
const ROOK_MOVE = 'R';
const PAWN_MOVE = '';

const PAWN_ICON = 'assets/pawn.png';
const KNIGHT_ICON = 'assets/knight.png';
const BISHOP_ICON = 'assets/bishop.png';
const ROOK_ICON = 'assets/rook.png';
const QUEEN_ICON = 'assets/queen.png';
const KING_ICON = 'assets/king.png';

const PAWN_DISABLED_ICON = 'assets/pawn_disabled.png';
const KNIGHT_DISABLED_ICON = 'assets/knight_disabled.png';
const BISHOP_DISABLED_ICON = 'assets/bishop_disabled.png';
const ROOK_DISABLED_ICON = 'assets/rook_disabled.png';
const QUEEN_DISABLED_ICON = 'assets/queen_disabled.png';
const KING_DISABLED_ICON = 'assets/king_disabled.png';

class PieceInfos {
  String label;
  String move;
  String icon;
  String iconDisabled;

  PieceInfos(
      {required this.label,
      required this.move,
      required this.icon,
      required this.iconDisabled}) {}
}

PieceInfos PAWN = PieceInfos(
    icon: PAWN_ICON,
    iconDisabled: PAWN_DISABLED_ICON,
    label: PAWN_LABEL,
    move: PAWN_MOVE);
PieceInfos KNIGHT = PieceInfos(
    icon: KNIGHT_ICON,
    iconDisabled: KNIGHT_DISABLED_ICON,
    label: KNIGHT_LABEL,
    move: KNIGHT_MOVE);
PieceInfos BISHOP = PieceInfos(
    icon: BISHOP_ICON,
    iconDisabled: BISHOP_DISABLED_ICON,
    label: BISHOP_LABEL,
    move: BISHOP_MOVE);
PieceInfos ROOK = PieceInfos(
    icon: ROOK_ICON,
    iconDisabled: ROOK_DISABLED_ICON,
    label: ROOK_LABEL,
    move: ROOK_MOVE);
PieceInfos QUEEN = PieceInfos(
    icon: QUEEN_ICON,
    iconDisabled: QUEEN_DISABLED_ICON,
    label: QUEEN_LABEL,
    move: QUEEN_MOVE);
PieceInfos KING = PieceInfos(
    icon: KING_ICON,
    iconDisabled: KING_DISABLED_ICON,
    label: KING_LABEL,
    move: KING_MOVE);

String getColor(Color color) {
  return color == Color.WHITE ? 'blanc' : 'noir';
}

String pieceTypeToLabel(Piece? piece) {
  if (piece == null) return 'case vide';
  switch (piece.type.name) {
    case 'p':
      return "${PAWN_LABEL} ${getColor(piece.color)}";
    case 'n':
      return "${KNIGHT_LABEL} ${getColor(piece.color)}";
    case 'b':
      return "${BISHOP_LABEL} ${getColor(piece.color)}";
    case 'r':
      return "${ROOK_LABEL} ${getColor(piece.color)}";
    case 'q':
      return "${QUEEN_LABEL} ${getColor(piece.color)}";
    case 'k':
      return "${KING_LABEL} ${getColor(piece.color)}";
    default:
      return 'case vide';
  }
}
