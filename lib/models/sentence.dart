import 'package:app/models/commands.dart';
import 'package:app/models/pieces.dart';
import 'package:app/models/tiles.dart';
import 'package:app/models/uci_move.dart';
import 'package:app/providers/game.dart';
import 'package:app/providers/tts.dart';
import 'package:chess/chess.dart';

class Sentence {
  String rawSentence;
  Game game;
  bool isLegal = false;
  String tile1 = '';
  String tile2 = '';
  String command = '';
  PieceInfos piece = PAWN;
  String? parsedMove;
  final TtsManager _ttsManager = TtsManager();

  Sentence({required this.rawSentence, required this.game}) {
    String sentenceParsed = parseAmbiguousWords(this.rawSentence.toLowerCase());
    List<String> tiles = getChessTiles()
        .where((string) => sentenceParsed.contains(string))
        .toList();
    command = CommandsArray.firstWhere(
        (string) => sentenceParsed.contains(string),
        orElse: () => "");
    piece = PiecesList.firstWhere(
        (piece) => sentenceParsed.contains(piece.label.toLowerCase()),
        orElse: () => PAWN);

    if (tiles.length > 0) {
      tile1 = tiles[0];
      if (tiles.length > 1) {
        tile2 = tiles[1];
      }
    }

    if (command.length == 0) {
      if (tile1.length == 0 || tile2.length == 0) {
        _ttsManager.speak('Commande invalide');
        parsedMove = "";
      } else {
        parsedMove =
            "${tile1}${tile2}${piece != null ? piece.move.toLowerCase() : ''}";
      }
    } else {
      parsedMove = "";
      if (command == 'case') {
        String tileContent = pieceTypeToLabel(game.chess.get(tile1));
        _ttsManager.speak((tileContent));
      } else if (command == 'légal') {
        UciMove uciMove = parseUCIMove("${tile1}${tile2}");
        bool legal = game.chess.move(uciMove.toJson());
        if (legal) {
          game.chess.undo();
          _ttsManager.speak('coup légal');
        } else {
          _ttsManager.speak('coup illégal');
        }
      }
    }
  }
}

String? moveToSentence(String move) {
  if (move == SHORT_CASTLE_MOVE) return SHORT_CASTLE_LABEL;
  if (move == LONG_CASTLE_MOVE) return LONG_CASTLE_LABEL;

  PieceInfos piece = PiecesListWithoutPawn.firstWhere(
      (element) => element.move == move[0],
      orElse: () => PAWN);
  if (piece != PAWN) {
    return move
        .replaceAll(piece.move, ' ${piece.label} ')
        .replaceAll('x', ' prend ')
        .replaceAll('+', ' échec ')
        .replaceAll('#', ' échec et matte ');
  } else {
    return "${PAWN.label} ${move.toLowerCase().replaceAll('x', ' prend ').replaceAll('+', ' échec ').replaceAll('=', 'promotion').replaceAll('#', ' échec et matte ')}";
  }
}
