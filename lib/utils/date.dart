String formatDate(String dateString) {
  List<String> parts = dateString.split(' ');
  String datePart = parts[0];
  List<String> dateParts = datePart.split('-');
  String formattedDate = '${dateParts[2]}-${dateParts[1]}-${dateParts[0]}';
  return formattedDate;
}
