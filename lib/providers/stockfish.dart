import 'package:app/models/move_played_return.dart';
import 'package:app/models/uci_move.dart';
import 'package:app/providers/game.dart';
import 'package:app/providers/iasingleton.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockfish/stockfish.dart';

typedef void EndCallback(MovePlayedReturn moveReturn);

class StockfishAI extends ChangeNotifier {
  IASingleton iaSingleton = IASingleton.instance;
  bool loading = false;
  UciMove? iaMove;
  Game game;
  late SharedPreferences prefs;
  EndCallback onEnd;

  get output {
    return iaSingleton.stockfish?.stdout ?? '';
  }

  get state {
    return iaSingleton.stockfish?.state.value ?? '';
  }

  Future<void> create() async {
    notifyListeners();
    await waitUntilState(StockfishState.ready);
    if (iaSingleton.stockfish != null) {
      iaSingleton.stockfish!.stdout.listen((event) {
        print('STOCKFISH: $event');
        if (event.contains('bestmove')) {
          final bestMove = event.split(' ')[1];
          UciMove uciMove = parseUCIMove(bestMove);
          iaMove = uciMove;
          MovePlayedReturn moveReturn = game.playComputerMove(bestMove);
          if (moveReturn.result != null) {
            onEnd(moveReturn);
          }
          notifyListeners();
        }
      });
    }
    notifyListeners();
  }

  Future<void> createStockfish() async {
    if (iaSingleton.stockfish != null) {
      iaSingleton.stockfish?.dispose();
      await waitUntilState(StockfishState.disposed);
    }
  }

  void sendCommand(String fen, String history) {
    notifyListeners();
    if (prefs != null) {
      int level = prefs.getInt('stockfish') ?? 1;
      iaSingleton.stockfish?.stdin =
          'setoption name Skill Level value ${level - 1}';
    }
    iaSingleton.stockfish?.stdin = 'position startpos moves ${history}';

    iaSingleton.stockfish?.stdin = 'go movetime 1500';
    notifyListeners();
  }

  void exit() {
    iaSingleton.stockfish?.stdin = 'exit';
    notifyListeners();
  }

  _loadPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  StockfishAI({required this.game, required this.onEnd}) {
    _loadPrefs();
  }

  Future<void> waitUntilState(StockfishState state) async {
    this.loading = true;
    notifyListeners();
    while (iaSingleton.stockfish?.state.value != state) {
      await Future.delayed(Duration(seconds: 1));
    }
    this.loading = false;
    notifyListeners();
  }
}
