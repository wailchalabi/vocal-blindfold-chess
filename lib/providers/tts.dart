import 'package:flutter_tts/flutter_tts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TtsManager {
  static final TtsManager _instance = TtsManager._internal();

  final FlutterTts _flutterTts;

  factory TtsManager() => _instance;

  late SharedPreferences prefs;

  _loadPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  TtsManager._internal() : _flutterTts = FlutterTts() {
    _loadPrefs();
    _flutterTts.setLanguage('fr-FR');
    _flutterTts.setSpeechRate(0.6);
    _flutterTts.setVolume(1.0);
  }

  Future<void> speak(String text) async {
    _loadPrefs();
    if (prefs.containsKey('soundOn') != null &&
        prefs.getBool('soundOn') == false) return;
    await Future.delayed(Duration(milliseconds: 300));
    _flutterTts.speak(text);
  }
}
