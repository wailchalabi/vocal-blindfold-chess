import 'package:stockfish/stockfish.dart';

class IASingleton {
  Stockfish? stockfish = Stockfish();

  IASingleton._();
  static final instance = IASingleton._();
}
