import 'package:app/models/move_played_return.dart';
import 'package:app/models/sentence.dart';
import 'package:app/models/uci_move.dart';
import 'package:app/providers/tts.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_chess_board/flutter_chess_board.dart';

String getLastMove(String pgn) {
  int lastSpaceIndex = pgn.lastIndexOf(' ');
  if (lastSpaceIndex != -1 && lastSpaceIndex < pgn.length - 1) {
    return pgn.substring(lastSpaceIndex + 1);
  }
  return '';
}

class Game extends ChangeNotifier {
  Chess chess = new Chess();
  PlayerColor? playerColor;
  String currentPgn = '1.';
  String lastMove = '';
  bool gameOn = false;
  bool iaTurn = PlayerColor == PlayerColor.black ? true : false;
  final TtsManager _ttsManager = TtsManager();

  Game._();
  static final instance = Game._();

  get historyString {
    return chess.history
        .map((e) =>
            "${e.move.fromAlgebraic}${e.move.toAlgebraic}${e.move.promotion?.name ?? ''}")
        .toList()
        .join(" ");
  }

  void startNewGame(PlayerColor color) {
    chess = new Chess();
    gameOn = true;
    playerColor = color;
    currentPgn = '1.';
    notifyListeners();
  }

  void stopGame() {
    gameOn = false;
    playerColor = null;
    currentPgn = '';
    notifyListeners();
  }

  MovePlayedReturn playPlayerMove(dynamic move) {
    bool legal = chess.move(move);

    if (legal) {
      currentPgn = chess.pgn();
      lastMove = getLastMove(currentPgn);
      String? vocalMove = moveToSentence(lastMove);
      if (vocalMove != null) {
        _ttsManager.speak((vocalMove));
      }
    }

    GameResult? res;
    if (chess.in_checkmate) res = GameResult.WON;
    if (chess.in_draw) res = GameResult.DRAW;
    if (chess.insufficient_material) res = GameResult.DRAW;
    if (chess.in_stalemate) res = GameResult.DRAW;
    if (chess.in_threefold_repetition) res = GameResult.DRAW;

    MovePlayedReturn moveReturn = MovePlayedReturn(isLegal: legal, result: res);
    iaTurn = true;
    notifyListeners();
    return moveReturn;
  }

  MovePlayedReturn playComputerMove(String move) {
    UciMove uciMove = parseUCIMove(move);
    bool legal = chess.move(uciMove.toJson());

    if (legal) {
      currentPgn = chess.pgn();
      lastMove = getLastMove(currentPgn);
      String? vocalMove = moveToSentence(lastMove);
      if (vocalMove != null) {
        _ttsManager.speak(('ordinateur joue ${vocalMove}'));
      }
    }
    GameResult? res;
    if (chess.in_checkmate) res = GameResult.LOOSE;
    if (chess.in_draw) res = GameResult.DRAW;
    if (chess.insufficient_material) res = GameResult.DRAW;
    if (chess.in_stalemate) res = GameResult.DRAW;
    if (chess.in_threefold_repetition) res = GameResult.DRAW;

    MovePlayedReturn moveReturn = MovePlayedReturn(isLegal: legal, result: res);
    iaTurn = false;
    notifyListeners();
    return moveReturn;
  }
}
