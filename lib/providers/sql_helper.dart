import 'dart:io';
import 'package:app/models/game_history.dart';
import 'package:app/models/move_played_return.dart';
import 'package:sqflite/sqflite.dart' as sql;

class SQLHelper {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE games(
      id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      pgn TEXT NOT NULL,
      result TEXT NOT NULL,
      difficulty INTEGER NOT NULL,
      color TEXT NOT NULL,
      moves INTEGER NOT NULL,
      createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP NOT NULL
    )
    """);
  }

  static Future<sql.Database> db() async {
    return sql.openDatabase('main.db', version: 1,
        onCreate: (sql.Database database, int version) async {
      await createTables(database);
    });
  }

  static Future<int> createGame(String pgn, String result, int? difficulty,
      String? color, int? moves) async {
    final db = await SQLHelper.db();
    final data = {
      'pgn': pgn,
      'result': result,
      'difficulty': difficulty,
      'color': color,
      'moves': moves
    };

    final id = await db.insert('games', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<void> deleteGame(int gameId) async {
    final db = await SQLHelper.db();

    // Delete the game with the given ID
    await db.delete('games', where: 'id = ?', whereArgs: [gameId]);

    // Close the database
    await db.close();
  }

  static Future<List<GameHistory>> getGamesHistory(
    List<String> excludedResults,
    List<String> excludedColors,
  ) async {
    final db = await SQLHelper.db();
    // Generate the WHERE clause condition dynamically based on excludedResults
    String whereClause = '';

    if (excludedResults.isNotEmpty) {
      whereClause +=
          excludedResults.map((result) => 'result <> ?').join(' AND ');
    }
    if (excludedColors.isNotEmpty) {
      if (excludedResults.isNotEmpty) {
        whereClause += ' AND ';
      }
      whereClause += excludedColors.map((color) => 'color <> ?').join(' AND ');
    }

    final result = await db.query('games',
        orderBy: 'id DESC',
        where: (excludedResults.isNotEmpty || excludedColors.isNotEmpty)
            ? whereClause
            : null,
        whereArgs: (excludedResults.isNotEmpty || excludedColors.isNotEmpty)
            ? [...excludedResults, ...excludedColors]
            : null);
    final list = result.toList();
    return GameHistory.fromJsonList(list);
  }

  static Future<void> deleteDatabaseFile() async {
    final databasePath = await sql.getDatabasesPath();
    final databaseFile = File('$databasePath/main.db');

    if (await databaseFile.exists()) {
      await databaseFile.delete();
    }
  }
}
