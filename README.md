# Jouer aux échecs à l'aveugle / Play Blindfold Chess

## Description

Flutter project allowing the user to play chess blindfold against the [open sourced AI Stockfish](https://stockfishchess.org/), and the [open sourced Lichess API](https://lichess.org/) for the games analysis

The production version deployed on Playstore is available [here](https://play.google.com/store/apps/developer?id=Totol+Toolsuite&hl=fr&gl=US)

This game is made for myself for my proper use, because I could'nt find an app answering to this need, to learn blindfold chess.

This project is educationnal made, to explore the Flutter's potential. If you have any request or idea for improving parts of the code base, feel free to submit or contact me at totol.toolsuite@gmail.com

## Limitations

The game is only available in french for the moment.

## Getting started

First run 

`flutter pub get` to get the packages

then
`flutter run` to run the app in local.

You should have a plugged android phone or and AVD Emulator running

### Change the icon
`flutter pub run flutter_launcher_icons `

### Change the splash screen
`dart run flutter_native_splash:create`

### Roadmap
- English version
- Adding a Player vs Player feature
- Improving the voice detection feature
